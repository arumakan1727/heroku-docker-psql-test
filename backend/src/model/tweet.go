package model

import (
	"fmt"
	"time"
)

type Tweet struct {
	ID        uint      `json:"id" gorm:"primaryKey;not null"`
	Message   string    `json:"message" gorm:"not null;size:255" binding:"required,max=255"`
	CreatedAt time.Time `json:"created_at" gorm:"not null"`
}

func (t Tweet) String() string {
	return fmt.Sprintf("Tweet{ID: %d, Message: %s, CreatedAt: %s}",
		t.ID, t.Message, t.CreatedAt)
}
