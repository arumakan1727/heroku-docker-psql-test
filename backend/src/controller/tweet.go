package controller

import (
	"log"
	"net/http"
	"time"

	"example.com/docker-psql-test/src/model"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

type ginHandler = func(*gin.Context)

func Hello(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "Hello world!",
	})
}

func PostNewTweet(db *gorm.DB) ginHandler {
	return func(c *gin.Context) {
		var tweet model.Tweet

		if err := c.ShouldBindJSON(&tweet); err != nil {
			log.Println("[Error] Failed to bind JSON: ", err)
			c.String(http.StatusBadRequest, "Invalid JSON: " + err.Error())
			return
		}
		log.Println("[Info] Succesfully parsed to JSON :", tweet)
		tweet.CreatedAt = time.Now()

		res := db.Create(&tweet)
		if res.Error != nil {
			log.Println("[Error] Failed to create record: ", res.Error)
			c.String(http.StatusInternalServerError, "Failed to create new tweet")
			return
		}
		log.Println("[Info] Succesfully created new tweet in DB:\n", tweet)

		c.JSON(http.StatusCreated, &tweet)
	}
}

func GetTweetList(db *gorm.DB) ginHandler {
	return func(c *gin.Context) {
		var tweets []model.Tweet
		db.Order("created_at desc").Find(&tweets)
		c.JSON(http.StatusOK, tweets)
	}
}
