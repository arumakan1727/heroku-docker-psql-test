package main

import (
	"database/sql"
	"log"
	"os"
	"time"

	"example.com/docker-psql-test/src/controller"
	"example.com/docker-psql-test/src/model"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	_ "github.com/lib/pq"
)

func main() {
	db := openDatabaseOrAbort(os.Getenv("DATABASE_URL"))
	migrateDatabaseOrAbort(db)

	if isRunlevelDebug() {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	r := gin.Default()
	r.GET("/api/hello", controller.Hello)
	r.POST("/api/tweet", controller.PostNewTweet(db))
	r.GET("/api/tweet", controller.GetTweetList(db))

	listen(r, os.Getenv("PORT"))
}

func listen(r *gin.Engine, port string) {
	if port == "" {
		port = "8080"
		log.Println("Warning: $PORT is empty, so using default port: 8080")
	}

	log.Fatal(r.Run(":" + port))
}

func getCustomGormLogger(out *os.File, isDebug bool) logger.Interface {
	logLevel := logger.Warn
	if isDebug {
		logLevel = logger.Info
	}

	return logger.New(
		log.New(out, "\n", log.LstdFlags),
		logger.Config{
			SlowThreshold:             time.Second,
			LogLevel:                  logLevel,
			IgnoreRecordNotFoundError: false,
			Colorful:                  true,
		},
	)
}

func isRunlevelDebug() bool {
	mode := os.Getenv("DEBUG_MODE")
	log.Println("[Info] $API_SERVER_RUN_MODE: ", mode)
	return mode == "1" || mode == "true" || mode == "yes" || mode == "on"
}

func openDatabaseOrAbort(dbUrl string) *gorm.DB {
	if dbUrl == "" {
		log.Fatalln("[Error] dbUrl is empty")
	}
	log.Println("[Info] Using dbUrl: " + dbUrl)

	sqlDB, err := sql.Open("postgres", dbUrl)
	if err != nil {
		log.Fatalln(err)
	}

	gormDB, err := gorm.Open(postgres.New(postgres.Config{
		Conn: sqlDB,
	}), &gorm.Config{
		Logger: getCustomGormLogger(os.Stderr, isRunlevelDebug()),
	})
	if err != nil {
		log.Fatalln(err)
	}

	return gormDB
}

func migrateDatabaseOrAbort(db *gorm.DB) {
	if err := db.AutoMigrate(&model.Tweet{}); err != nil {
		log.Fatal(err)
	}
	log.Println("[Info] Successfully migrated: model.Tweet")
}
